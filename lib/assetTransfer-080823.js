/*
 * Copyright IBM Corp. All Rights Reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');

//const Client = require('fabric-client');
const fs = require("fs");

//const State = require('../ledger-api/state.js');

//////////////////////////////////////////////////////////////////////////////////////////////
// Events management

const EventEmitter = require('events');

const emitter = new EventEmitter();

// register all listeners
/*
emitter.on('event createStock', (info) => {console.info('Event Listener createStock called : ' + info);});	
emitter.on('event createContrat', (info) => {console.info('Event Listener createContrat called : ' + info);});	
emitter.on('event checkAvailability', (info) => {console.info('Event Listener checkAvailability called : ' + info);});	
emitter.on('event validClient', (info) => {console.info('Event Listener validClient called : ' + info);});	
emitter.on('event progressDelivery', (info) => {console.info('Event Listener progressDelivery called : ' + info);});	
emitter.on('event finalizeDelivery', (info) => {console.info('Event Listener finalizeDelivery called : ' + info);});	
*/
/*
checkAvailability","Args":["1649855289","stockYAM1","contratYAM1","30"]}'
validClient","Args":["1649855289","stockYAM1","contratYAM1","30"]}'
progressDelivery","Args":["1649855289","stockYAM1","contratYAM1","30"]}'
finalizeDelivery","Args":["1649855289","stockYAM1","contratYAM1","30"]}'
*/

//////////////////////////////////////////////////////////////////////////////////////////////


class AssetTransfer extends Contract {

    async InitLedger(ctx) {
        const assets = [
            {
                ID: 'asset1',
                Color: 'blue',
                Size: 5,
                Owner: 'Tomoko',
                AppraisedValue: 300,
            },
            {
                ID: 'asset2',
                Color: 'red',
                Size: 5,
                Owner: 'Brad',
                AppraisedValue: 400,
            },
            {
                ID: 'asset3',
                Color: 'green',
                Size: 10,
                Owner: 'Jin Soo',
                AppraisedValue: 500,
            },
            {
                ID: 'asset4',
                Color: 'yellow',
                Size: 10,
                Owner: 'Max',
                AppraisedValue: 600,
            },
            {
                ID: 'asset5',
                Color: 'black',
                Size: 15,
                Owner: 'Adriana',
                AppraisedValue: 700,
            },
            {
                ID: 'asset6',
                Color: 'white',
                Size: 15,
                Owner: 'Michel',
                AppraisedValue: 800,
            },
        ];

        for (const asset of assets) {
            asset.docType = 'asset';
            await ctx.stub.putState(asset.ID, Buffer.from(JSON.stringify(asset)));
            console.info(`Asset ${asset.ID} initialized`);
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////////////

    async getHistory(ctx, key) {

        const promiseOfIterator = ctx.stub.getHistoryForKey(key);

        const results = [];
        for await (const keyMod of promiseOfIterator) {
            console.log('keymod: ' + JSON.stringify(keyMod));
            const ts = keyMod.timestamp;
            //const milliseconds = (ts.seconds.low + ((ts.nanos / 1000000) / 1000)) * 1000;

            console.log('seconds: ' + ts.seconds);
            console.log('nanos: ' + ts.nanos);

            const milliseconds = (ts.seconds + ((ts.nanos / 1000000) / 1000)) * 1000;

            console.log('milliseconds: ' + milliseconds);

            const date = new Date(milliseconds);
            var milli = date.getMilliseconds();
            var timeString = date.toLocaleTimeString() + ":" + milliseconds;

            console.log('date: ' + timeString);


            var day = date.getDate();
            var month = date.getMonth() + 1; // Ajouter 1 car les mois commencent à partir de 0
            var year = date.getFullYear();
            var hours = date.getHours();
            var minutes = date.getMinutes();
            var seconds = date.getSeconds();
            var weekdays = ['dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
            var months = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre'];

            // milli = xxx
            var nano1 = ts.nanos; // xxx yyy zzz
            var nano2 = nano1 - ( milli * 1000000); //  yyy zzz
            var micro = nano2 / 1000; // yyy

            micro = Math.trunc(micro); // supprime les décimales

            var nano3 = nano2 - ( micro * 1000 ); // zzz

            var dateString = weekdays[date.getDay()] + " " + day + " " + months[month - 1] + " " + year + " à " + hours + "h " + minutes + "m " + seconds + "s " 
            + milli + "ms "+ micro + "µs "+nano3+ "ns ";
            console.log(dateString);

            const transid = keyMod.txId;
            console.log('txId: ' + transid);
            const val = keyMod.value.toString('utf8');
            //const val = keyMod.value;

            console.log('value: ' + val);

            let dateLocale = date.toLocaleString('fr-FR', {
                weekday: 'long',
                year: 'numeric',
                month: 'long',
                day: 'numeric',
                hour: 'numeric',
                minute: 'numeric',
                second: 'numeric'
            });

            const resp = {
                //timestamp:date,			
                //timestamp:keyMod.timestamp,		
                value: val,
                timestamp: dateString,
                txid: transid
            }
            results.push(resp);
        }
        // results array contains the key history
        return results;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////

    async getEnv(ctx, key) {

        const bind = ctx.stub.getBinding(key);
        console.log('getBinding: ' + bind);

        const creat = ctx.stub.getCreator(key);
        console.log('getCreator: ' + JSON.stringify(creat));

        const msp = ctx.stub.getMspID(key);
        console.log('getMspID: ' + msp);

        const trans = ctx.stub.getTransient(key);
        console.log('getTransient: ' + JSON.stringify(trans));

        const txid = ctx.stub.getTxID(key);
        console.log('getTxID: ' + txid);

        const resp = {
            Transient: trans,
            Txid: txid,
            Binding: bind,
            Creator: creat
            //	    Msp : msp,

        }

        return resp;
    }


    //////////////////////////////////////////////////////////////////////////////////////////////

    // createStock issues a new stock to the world state with given details.
    //async createStock(ctx, date, stockID, stock, enLivraison) {
    async createStock(ctx, date, stockID, userFournisseur, entrepriseFournisseur, id_marchandise, stock, enLivraison,
        id_contrat, userClient, entrepriseClient) {

        var exists = await this.stockExists(ctx, stockID);
        if (exists) {
            throw new Error(`The stock ${stockID} already exists`);
        }
        /*        
                exists = await this.contratExists(ctx, id_contrat);
                if (!exists) {
                    throw new Error(`The contrat ${id_contrat} does not exist`);
                }
        */
        exists = await this.userExists(ctx, userFournisseur);
        if (!exists) {
            throw new Error(`The userFournisseur ${userFournisseur} does not exist`);
        }
        exists = await this.entrepriseExists(ctx, entrepriseFournisseur);
        if (!exists) {
            throw new Error(`The entrepriseFournisseur ${entrepriseFournisseur} does not exist`);
        }
        exists = await this.marchandiseExists(ctx, id_marchandise);
        if (!exists) {
            throw new Error(`The id_marchandise ${id_marchandise} does not exist`);
        }
        /*        
                exists = await this.userExists(ctx, userClient);
                if (!exists) {
                    throw new Error(`The userClient ${userClient} does not exist`);
                }
                exists = await this.entrepriseExists(ctx, entrepriseClient);
                if (!exists) {
                    throw new Error(`The entrepriseClient ${entrepriseClient} does not exist`);
                }
        */
        console.log('createStock date: ' + date);

        const Stoc = {
            StockID: stockID,
            Datestock: date,
 //           Datestock: "",
            Stock: stock,
            EnLivraison: enLivraison,
            UserFournisseur: userFournisseur,
            EntrepriseFournisseur: entrepriseFournisseur,
            Id_marchandise: id_marchandise,
            EnLivraison: enLivraison,
            Id_contrat: id_contrat,
            UserClient: userClient,
            EntrepriseClient: entrepriseClient
        };

        const stockBuffer = Buffer.from(JSON.stringify(Stoc));
        ctx.stub.setEvent('createStock', stockBuffer);

        await ctx.stub.putState(stockID, stockBuffer);
        console.info(`INFO YAM createStock`);

        // raise an Event
        emitter.emit('event createStock', stockBuffer);


        /*		
                try {			
                await fs.promises.writeFile(`/YAM.txt`, `SALUT createStock`);
                } catch (err) {
                console.error(`Erreur ecriture createStock`);
                } 
            	
        
                // check to see if there is a config json defined
                if (fs.existsSync(addAssetsConfigFile)) {
                    // read file the next asset and number of assets to create
                    let addAssetsConfigJSON = fs.readFileSync(addAssetsConfigFile, 'utf8');
                    addAssetsConfig = JSON.parse(addAssetsConfigJSON);
                    nextAssetNumber = addAssetsConfig.nextAssetNumber;
                    numberAssetsToAdd = addAssetsConfig.numberAssetsToAdd;
                } else {
                    nextAssetNumber = 100;
                    numberAssetsToAdd = 20;
                    // create a default config and save
                    addAssetsConfig = new Object;
                    addAssetsConfig.nextAssetNumber = nextAssetNumber;
                    addAssetsConfig.numberAssetsToAdd = numberAssetsToAdd;
                    fs.writeFileSync(addAssetsConfigFile, JSON.stringify(addAssetsConfig, null, 2));
                }
            	
            	
            	
                const addAssetsConfigFile = path.resolve(__dirname, 'addAssets.json');
            	
                fs.writeFileSync(addAssetsConfigFile, JSON.stringify(addAssetsConfig, null, 2));		
            	
        */
        return JSON.stringify(Stoc);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////

    // createContrat issues a new contrat to the world state with given details.
    //async createContrat(ctx, date, contratID, statut, quantite) {	
    async createContrat(ctx, date, contratID, statut, quantite,
        userClient, entrepriseClient, id_marchandise, id_stock, userFournisseur, entrepriseFournisseur) {

        var exists = await this.contratExists(ctx, contratID);
        if (exists) {
            throw new Error(`The contrat ${contratID} already exists`);
        }
        exists = await this.userExists(ctx, userClient);
        if (!exists) {
            throw new Error(`The userClient ${userClient} does not exist`);
        }
        exists = await this.entrepriseExists(ctx, entrepriseClient);
        if (!exists) {
            throw new Error(`The entrepriseClient ${entrepriseClient} does not exist`);
        }
        exists = await this.marchandiseExists(ctx, id_marchandise);
        if (!exists) {
            throw new Error(`The id_marchandise ${id_marchandise} does not exist`);
        }

        const contrat = {
            ContratID: contratID,
            Datecontrat: date,
            Statut: statut,
            Quantite: quantite,
            UserClient: userClient,
            EntrepriseClient: entrepriseClient,
            Id_marchandise: id_marchandise,
            Id_stock: id_stock,
            UserFournisseur: userFournisseur,
            EntrepriseFournisseur: entrepriseFournisseur
        };

        const stockContrat = Buffer.from(JSON.stringify(contrat));

        await ctx.stub.putState(contratID, stockContrat);
        console.info(`INFO YAM createContrat`);

        // raise an Event
        emitter.emit('event createContrat', stockContrat);

        /*			
                try {			
                await fs.promises.writeFile(`/YAM.txt`, `SALUT createContrat`);
                } catch (err) {
                console.error(`Erreur ecriture createContrat`);
                } 
        */
        return JSON.stringify(contrat);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////

    async createUser(ctx, date, userID, type, entreprise, password) {

        const exists = await this.userExists(ctx, userID);
        if (exists) {
            throw new Error(`The user ${userID} already exists`);
        }
        const now = new Date();
        console.log('createUser now: ' + now);
        //        const milliseconds = (date.seconds.low + ((date.nanos / 1000000) / 1000)) * 1000;
        const milliseconds = (date) * 1000;
        const dateconv = new Date(milliseconds);
        console.log('dateconv: ' + dateconv);

        //var d = new Date();
        //var datestring = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();

        const UserB = {
            UserID: userID,
            DateUser: date,
            Type: type,
            Entreprise: entreprise,
            Password: password,
        };

        const userBuffer = Buffer.from(JSON.stringify(UserB));

        //ctx.stub.setEvent('createUser', userBuffer);

        await ctx.stub.putState(userID, userBuffer);
        console.info(`INFO YAM createUser`);

        // raise an Event
        //emitter.emit('event createUser', userBuffer );				

        return JSON.stringify(UserB);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////

    async createEntreprise(ctx, date, entrepriseID) {

        const exists = await this.entrepriseExists(ctx, entrepriseID);
        if (exists) {
            throw new Error(`The entreprise ${entrepriseID} already exists`);
        }
        const now = new Date();
        console.log('createEntreprise now: ' + now);
        //        const milliseconds = (date.seconds.low + ((date.nanos / 1000000) / 1000)) * 1000;
        const milliseconds = (date) * 1000;
        const dateconv = new Date(milliseconds);
        console.log('dateconv: ' + dateconv);

        //var d = new Date();
        //var datestring = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();

        const Entreprise = {
            EntrepriseID: entrepriseID,
            DateEntreprise: date,
            //DateEntreprise : now,						
        };

        const entrepriseBuffer = Buffer.from(JSON.stringify(Entreprise));

        //ctx.stub.setEvent('createEntreprise', entrepriseBuffer);

        await ctx.stub.putState(entrepriseID, entrepriseBuffer);
        console.info(`INFO YAM createEntreprise`);

        // raise an Event
        //emitter.emit('event createEntreprise', entrepriseBuffer );				

        return JSON.stringify(Entreprise);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////

    async createMarchandise(ctx, date, marchandiseID) {

        const exists = await this.marchandiseExists(ctx, marchandiseID);
        if (exists) {
            throw new Error(`The marchandise ${marchandiseID} already exists`);
        }
        const now = new Date();
        console.log('createMarchandise now: ' + now);
        //        const milliseconds = (date.seconds.low + ((date.nanos / 1000000) / 1000)) * 1000;
        const milliseconds = (date) * 1000;
        const dateconv = new Date(milliseconds);
        console.log('dateconv: ' + dateconv);

        //var d = new Date();
        //var datestring = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();

        const Marchandise = {
            MarchandiseID: marchandiseID,
            DateMarchandise: date,
            //DateMarchandise : now,						
        };

        const marchandiseBuffer = Buffer.from(JSON.stringify(Marchandise));

        //ctx.stub.setEvent('createMarchandise', marchandiseBuffer);

        await ctx.stub.putState(marchandiseID, marchandiseBuffer);
        console.info(`INFO YAM createMarchandise`);

        // raise an Event
        //emitter.emit('event createMarchandise', marchandiseBuffer );				

        return JSON.stringify(Marchandise);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////

    // CreateAsset issues a new asset to the world state with given details.
    async CreateAsset(ctx, id, color, size, owner, appraisedValue) {

        const exists = await this.AssetExists(ctx, id);
        if (exists) {
            throw new Error(`The asset ${id} already exists`);
        }

        const asset = {
            ID: id,
            Color: color,
            Size: size,
            Owner: owner,
            AppraisedValue: appraisedValue,
        };

        await ctx.stub.putState(id, Buffer.from(JSON.stringify(asset)));
        console.info(`INFO YAM CreateAsset`);

        try {
            await fs.promises.writeFile(`/YAM.txt`, `SALUT CreateAsset`);
        } catch (err) {
            console.error(`Erreur ecriture`);
        }

        return JSON.stringify(asset);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////

    // readStock returns the stock stored in the world state with given id.
    async readStock(ctx, id) {
        const now = new Date();
        console.log('readStock now: ' + now);
        const stockJSON = await ctx.stub.getState(id); // get the stock from chaincode state
        if (!stockJSON || stockJSON.length === 0) {
            throw new Error(`The stock ${id} does not exist`);
        }
        console.log('stockJSON: ' + stockJSON.toString());
        return stockJSON.toString();
    }

    //////////////////////////////////////////////////////////////////////////////////////////////

    // readContrat returns the contrat stored in the world state with given id.
    async readContrat(ctx, id) {
        const now = new Date();
        console.log('readContrat now: ' + now);
        const contratJSON = await ctx.stub.getState(id); // get the contrat from chaincode state
        if (!contratJSON || contratJSON.length === 0) {
            throw new Error(`The contrat ${id} does not exist`);
        }
        console.log('contratJSON: ' + contratJSON.toString());
        return contratJSON.toString();
    }

    //////////////////////////////////////////////////////////////////////////////////////////////

    // readUser returns the user stored in the world state with given id.
    async readUser(ctx, id) {
        const now = new Date();
        console.log('readUser now: ' + now);
        const userJSON = await ctx.stub.getState(id); // get the user from chaincode state
        if (!userJSON || userJSON.length === 0) {
            throw new Error(`The user ${id} does not exist`);
        }
        console.log('userJSON: ' + userJSON.toString());
        return userJSON.toString();
    }

    //////////////////////////////////////////////////////////////////////////////////////////////

    // readEntreprise returns the entreprise stored in the world state with given id.
    async readEntreprise(ctx, id) {
        const now = new Date();
        console.log('readEntreprise now: ' + now);
        const entrepriseJSON = await ctx.stub.getState(id); // get the entreprise from chaincode state
        if (!entrepriseJSON || entrepriseJSON.length === 0) {
            throw new Error(`The entreprise ${id} does not exist`);
        }
        console.log('entrepriseJSON: ' + entrepriseJSON.toString());
        return entrepriseJSON.toString();
    }

    //////////////////////////////////////////////////////////////////////////////////////////////

    // readMarchandise returns the marchandise stored in the world state with given id.
    async readMarchandise(ctx, id) {
        const now = new Date();
        console.log('readMarchandise now: ' + now);
        const marchandiseJSON = await ctx.stub.getState(id); // get the marchandise from chaincode state
        if (!marchandiseJSON || marchandiseJSON.length === 0) {
            throw new Error(`The marchandise ${id} does not exist`);
        }
        console.log('marchandiseJSON: ' + marchandiseJSON.toString());
        return marchandiseJSON.toString();
    }

    //////////////////////////////////////////////////////////////////////////////////////////////

    // getContratStatus returns the contrat status stored in the world state with given id.
    async getContratStatus(ctx, id) {
        const now = new Date();
        console.log('getContratStatus now: ' + now);
        const contratJSON = await ctx.stub.getState(id); // get the contrat from chaincode state
        if (!contratJSON || contratJSON.length === 0) {
            throw new Error(`The contrat ${id} does not exist`);
        }
        const contratOBJ = JSON.parse(contratJSON);
        const statutActuel = contratOBJ.Statut;
        console.log('statutActuel: ' + statutActuel);
        return statutActuel;

    }

    //////////////////////////////////////////////////////////////////////////////////////////////

    // getContratQuantity returns the contrat quantity stored in the world state with given id.
    async getContratQuantity(ctx, id) {
        const now = new Date();
        console.log('getContratQuantity now: ' + now);
        const contratJSON = await ctx.stub.getState(id); // get the contrat from chaincode state
        if (!contratJSON || contratJSON.length === 0) {
            throw new Error(`The contrat ${id} does not exist`);
        }
        const contratOBJ = JSON.parse(contratJSON);
        const quantityActuel = contratOBJ.Quantite;
        console.log('quantityActuel: ' + quantityActuel);
        return quantityActuel;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////

    // getAssetStock returns the stock  Stock stored in the world state with given id.
    async getAssetStock(ctx, id) {
        const now = new Date();
        console.log('getAssetStock now: ' + now);
        const stockJSON = await ctx.stub.getState(id); // get the stock from chaincode state
        if (!stockJSON || stockJSON.length === 0) {
            throw new Error(`The stock ${id} does not exist`);
        }
        const stockOBJ = JSON.parse(stockJSON);
        const stockActuel = stockOBJ.Stock;
        console.log('stockActuel: ' + stockActuel);
        return stockActuel;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////

    // getUserEntreprise returns the entreprise stored in the world state with given id.
    async getUserEntreprise(ctx, id) {
        const now = new Date();
        console.log('getUserEntreprise now: ' + now);
        const userJSON = await ctx.stub.getState(id); // get the user from chaincode state
        if (!userJSON || userJSON.length === 0) {
            throw new Error(`The user ${id} does not exist`);
        }
        const userOBJ = JSON.parse(userJSON);
        const entrepriseActuel = userOBJ.Entreprise;
        console.log('entrepriseActuel: ' + entrepriseActuel);
        return entrepriseActuel;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////

    // getUserType returns the type stored in the world state with given id.
    async getUserType(ctx, id) {
        const now = new Date();
        console.log('getUserType now: ' + now);
        const userJSON = await ctx.stub.getState(id); // get the user from chaincode state
        if (!userJSON || userJSON.length === 0) {
            throw new Error(`The user ${id} does not exist`);
        }
        const userOBJ = JSON.parse(userJSON);
        const typeActuel = userOBJ.Type;
        console.log('typeActuel: ' + typeActuel);
        return typeActuel;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////

    // getAssetEnLivraison returns the stock  EnLivraison stored in the world state with given id.
    async getAssetEnLivraison(ctx, id) {
        const now = new Date();
        console.log('getAssetEnLivraison now: ' + now);
        const stockJSON = await ctx.stub.getState(id); // get the stock from chaincode state
        if (!stockJSON || stockJSON.length === 0) {
            throw new Error(`The stock ${id} does not exist`);
        }
        const stockOBJ = JSON.parse(stockJSON);
        const enlivraisonActuel = stockOBJ.EnLivraison;
        console.log('enlivraisonActuel: ' + enlivraisonActuel);
        return enlivraisonActuel;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////

    // ReadAsset returns the asset stored in the world state with given id.
    async ReadAsset(ctx, id) {
        const assetJSON = await ctx.stub.getState(id); // get the asset from chaincode state
        if (!assetJSON || assetJSON.length === 0) {
            throw new Error(`The asset ${id} does not exist`);
        }
        return assetJSON.toString();
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    /**
      * augmentAssetStock.
      *
      * @param ctx the transaction context
      * @param stockID the ID of the stock
      * @param stockToadd the quantity to add
      * @return the stock found on the ledger if there was one
      */

    // augmentAssetStock updates an existing stock in the world state with provided parameters.
    // ctx the transaction context
    // stockID the ID of the stock
    // stockToadd the quantity to add	


    async augmentAssetStock(ctx, date, stockID, stockToadd) {
        const now = new Date();
        console.log('augmentAssetStock now: ' + now);
        const exists = await this.stockExists(ctx, stockID);
        if (!exists) {
            throw new Error(`The stock ${stockID} does not exist`);
        }
        const stockJSON = await ctx.stub.getState(stockID); // get the stock from chaincode state
        const stockOBJ = JSON.parse(stockJSON);
        const stockActuel = stockOBJ.Stock;
        console.log('stockActuel: ' + stockActuel);

        //var d = new Date();
        //var datestring = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();

        const updatedStock = {
            StockID: stockID,
            Datestock: date,
            Stock: String(parseInt(stockActuel) + parseInt(stockToadd)),
            EnLivraison: stockOBJ.EnLivraison,
        };

        await ctx.stub.putState(stockID, Buffer.from(JSON.stringify(updatedStock)));
        console.info(`INFO YAM augmentAssetStock`);

        return JSON.stringify(updatedStock);

    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    /**
      * resetStock
      *
      * @param ctx the transaction context
      * @param stockID the ID of the stock
      * @return the stock found on the ledger if there was one
      */

    // resetStock reset an existing stock in the world state .
    // ctx the transaction context
    // stockID the ID of the stock

    async resetStock(ctx, date, stockID) {
        const now = new Date();
        console.log('resetStock now: ' + now);
        const exists = await this.stockExists(ctx, stockID);
        if (!exists) {
            throw new Error(`The stock ${stockID} does not exist`);
        }
        const stockJSON = await ctx.stub.getState(stockID); // get the stock from chaincode state
        const stockOBJ = JSON.parse(stockJSON);
        const stockActuel = stockOBJ.Stock;
        console.log('stockActuel: ' + stockActuel);

        const userFournisseurActuel = stockOBJ.UserFournisseur;
        const entrepriseFournisseurActuel = stockOBJ.EntrepriseFournisseur;
        const id_marchandiseActuel = stockOBJ.Id_marchandise;

        const updatedStock = {
            StockID: stockID,
            Datestock: date,
            Stock: stockActuel,
            EnLivraison: "0",
            UserFournisseur: userFournisseurActuel,
            EntrepriseFournisseur: entrepriseFournisseurActuel,
            Id_marchandise: id_marchandiseActuel,
            Id_contrat: "",
            UserClient: "",
            EntrepriseClient: ""
        };

        await ctx.stub.putState(stockID, Buffer.from(JSON.stringify(updatedStock)));
        console.info(`INFO YAM resetStock`);

        return JSON.stringify(updatedStock);

    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    /**
      * resetContrat
      *
      * @param ctx the transaction context
      * @param contratID the ID of the contrat
      * @return the contrat found on the ledger if there was one
      */

    // resetContrat reset an existing contrat in the world state .

    async resetContrat(ctx, date,contratID) {

        const now = new Date();
        console.log('resetContrat now: ' + now);

        const contratexists = await this.contratExists(ctx, contratID);
        if (!contratexists) {
            throw new Error(`The contrat ${contratID} does not exist`);
        }

        const contratJSON = await ctx.stub.getState(contratID); // get the contrat from chaincode state
        const contratOBJ = JSON.parse(contratJSON);
        const dateActuel = contratOBJ.Datecontrat;
        const quantityActuel = contratOBJ.Quantite;
        const userClientActuel = contratOBJ.UserClient;
        const entrepriseClientActuel = contratOBJ.EntrepriseClient;
        const id_marchandiseActuel = contratOBJ.Id_marchandise;
        const id_stockActuel = contratOBJ.Id_stock;
        const userFournisseurActuel = contratOBJ.UserFournisseur;
        const entrepriseFournisseurActuel = contratOBJ.EntrepriseFournisseur;

        console.log('quantityActuel: ' + quantityActuel);

        const contrat = {
            ContratID: contratID,
            Datecontrat: date,
            Statut: 'SANS_COMMANDE',
            Quantite: "0",
            UserClient: userClientActuel,
            EntrepriseClient: entrepriseClientActuel,
            Id_marchandise: id_marchandiseActuel,
            Id_stock: "",
            UserFournisseur: "",
            EntrepriseFournisseur: ""
        };

        const stockContrat = Buffer.from(JSON.stringify(contrat));
        await ctx.stub.putState(contratID, stockContrat);
        console.info(`INFO YAM resetContrat`);
        return JSON.stringify(contrat);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////

    // UpdateAsset updates an existing asset in the world state with provided parameters.
    async UpdateAsset(ctx, id, color, size, owner, appraisedValue) {
        const exists = await this.AssetExists(ctx, id);
        if (!exists) {
            throw new Error(`The asset ${id} does not exist`);
        }

        // overwriting original asset with new asset
        const updatedAsset = {
            ID: id,
            Color: color,
            Size: size,
            Owner: owner,
            AppraisedValue: appraisedValue,
        };

        return ctx.stub.putState(id, Buffer.from(JSON.stringify(updatedAsset)));
    }

    // deleteAsset deletes an given asset from the world state.
    async deleteAsset(ctx, id) {
        const exists = await this.AssetExists(ctx, id);
        if (!exists) {
            throw new Error(`The asset ${id} does not exist`);
        }
        return ctx.stub.deleteState(id);
    }
    ////////////////////////////////////////////////////////////////////////////////

    // updateStock updates an existing stock in the world state with provided parameters.

    async updateStock(ctx, date, stockID, stock, enLivraison, id_marchandise) {
        const now = new Date();
        console.log('updateStock now: ' + now);
        const exists = await this.stockExists(ctx, stockID);
        if (!exists) {
            throw new Error(`The stock ${stockID} does not exist`);
        }
        // overwriting original stock with new stock

        //var d = new Date();
        //var datestring = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();

        const stockJSON = await ctx.stub.getState(stockID); // get the stock from chaincode state
        const stockOBJ = JSON.parse(stockJSON);
        const stockActuel = stockOBJ.Stock;
        console.log('stockActuel: ' + stockActuel);

        const updatedStock = {
            StockID: stockID,
            Datestock: date,
            Stock: stock,
            EnLivraison: enLivraison,
            UserFournisseur: stockOBJ.UserFournisseur,
            EntrepriseFournisseur: stockOBJ.EntrepriseFournisseur,
            Id_marchandise: id_marchandise,
            Id_contrat: stockOBJ.Id_contrat,
            UserClient: stockOBJ.UserClient,
            EntrepriseClient: stockOBJ.EntrepriseClient
        };

        const stockBuffer = Buffer.from(JSON.stringify(updatedStock));
        ctx.stub.setEvent('updateStock', stockBuffer);

        await ctx.stub.putState(stockID, stockBuffer);

        console.info(`INFO YAM updateStock`);
        return JSON.stringify(updatedStock);
    }

    ////////////////////////////////////////////////////////////////////////////////

    // updateMarchandiseStock updates an existing stock in the world state with provided parameters.

    async updateMarchandiseStock(ctx, date, stockID, id_marchandise) {
        const now = new Date();
        console.log('updateStock now: ' + now);
        const exists = await this.stockExists(ctx, stockID);
        if (!exists) {
            throw new Error(`The stock ${stockID} does not exist`);
        }
        // overwriting original stock with new stock

        //var d = new Date();
        //var datestring = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();

        const stockJSON = await ctx.stub.getState(stockID); // get the stock from chaincode state
        const stockOBJ = JSON.parse(stockJSON);
        const stockActuel = stockOBJ.Stock;
        console.log('stockActuel: ' + stockActuel);

        const updatedStock = {
            StockID: stockID,
            Datestock: date,
            Stock: stockOBJ.Stock,
            EnLivraison: stockOBJ.EnLivraison,
            UserFournisseur: stockOBJ.UserFournisseur,
            EntrepriseFournisseur: stockOBJ.EntrepriseFournisseur,
            Id_marchandise: id_marchandise,
            Id_contrat: stockOBJ.Id_contrat,
            UserClient: stockOBJ.UserClient,
            EntrepriseClient: stockOBJ.EntrepriseClient
        };

        const stockBuffer = Buffer.from(JSON.stringify(updatedStock));
        ctx.stub.setEvent('updateStock', stockBuffer);

        await ctx.stub.putState(stockID, stockBuffer);

        console.info(`INFO YAM updateStock`);
        return JSON.stringify(updatedStock);
    }

    ////////////////////////////////////////////////////////////////////////////////    

    // updateMarchandiseContrat updates an existing contrat in the world state with provided parameters.

    async updateMarchandiseContrat(ctx, date, contratID, id_marchandise) {

        const now = new Date();
        console.log('updateMarchandiseContrat now: ' + now);

        const exists = await this.contratExists(ctx, contratID);
        if (!exists) {
            throw new Error(`The contrat ${contratID} does not exist`);
        }
 
        const exists2 = await this.marchandiseExists(ctx, id_marchandise);
        if (!exists2) {
            throw new Error(`The marchandise ${id_marchandise} does not exist`);
        }       
        // overwriting original contrat with new contrat

        const contratJSON = await ctx.stub.getState(contratID); // get the contrat from chaincode state
        const contratOBJ = JSON.parse(contratJSON);

        const updatedContrat = {
            ContratID: contratID,
            Datecontrat: date,
            Statut: contratOBJ.Statut,
            Quantite: contratOBJ.Quantite,
            UserClient: contratOBJ.UserClient,
            EntrepriseClient: contratOBJ.EntrepriseClient,
            Id_marchandise: id_marchandise,
            Id_stock: contratOBJ.Id_stock,
            UserFournisseur: contratOBJ.UserFournisseur,
            EntrepriseFournisseur: contratOBJ.EntrepriseFournisseur
        };

        const contratBuffer = Buffer.from(JSON.stringify(updatedContrat));
        ctx.stub.setEvent('updateMarchandiseContrat', contratBuffer);

        await ctx.stub.putState(contratID, contratBuffer);

        console.info(`INFO YAM updateMarchandiseContrat`);
        return JSON.stringify(updatedContrat);
    }

    ////////////////////////////////////////////////////////////////////////////////    

    // updateTypeUser updates an existing user in the world state with provided parameters.

    async updateTypeUser(ctx, date, userID, type) {

        const now = new Date();
        console.log('updateTypeUser now: ' + now);

        const exists = await this.userExists(ctx, userID);
        if (!exists) {
            throw new Error(`The user ${userID} does not exist`);
        }
 
        /*
        const exists2 = await this.typeExists(ctx, type);
        if (!exists2) {
            throw new Error(`The type ${type} does not exist`);
        }   
        */    

        // overwriting original user with new user

        const userJSON = await ctx.stub.getState(userID); // get the user from chaincode state
        const userOBJ = JSON.parse(userJSON);

        const updatedUser = {
            UserID: userID,
            DateUser: date,
            Type: type,
            Entreprise: userOBJ.Entreprise,
            Password: userOBJ.Password,
        };

        const userBuffer = Buffer.from(JSON.stringify(updatedUser));
        ctx.stub.setEvent('updateTypeUser', userBuffer);

        await ctx.stub.putState(userID, userBuffer);

        console.info(`INFO YAM updateTypeUser`);
        return JSON.stringify(updatedUser);
    }

    ////////////////////////////////////////////////////////////////////////////////    

    // updateEntrepriseUser updates an existing user in the world state with provided parameters.

    async updateEntrepriseUser(ctx, date, userID, entreprise) {

        const now = new Date();
        console.log('updateEntrepriseUser now: ' + now);

        const exists = await this.userExists(ctx, userID);
        if (!exists) {
            throw new Error(`The user ${userID} does not exist`);
        }
 
        const exists2 = await this.entrepriseExists(ctx, entreprise);
        if (!exists2) {
            throw new Error(`The entreprise ${entreprise} does not exist`);
        }      

        // overwriting original user with new user

        const userJSON = await ctx.stub.getState(userID); // get the user from chaincode state
        const userOBJ = JSON.parse(userJSON);

        const updatedUser = {
            UserID: userID,
            DateUser: date,
            Type: userOBJ.Type,
            Entreprise: entreprise,
            Password: userOBJ.Password,
        };

        const userBuffer = Buffer.from(JSON.stringify(updatedUser));
        ctx.stub.setEvent('updateEntrepriseUser', userBuffer);

        await ctx.stub.putState(userID, userBuffer);

        console.info(`INFO YAM updateEntrepriseUser`);
        return JSON.stringify(updatedUser);
    }

    ////////////////////////////////////////////////////////////////////////////////    

    // updatePasswordUser updates an existing user in the world state with provided parameters.

    async updatePasswordUser(ctx, date, userID, password) {

        const now = new Date();
        console.log('updatePasswordUser now: ' + now);

        const exists = await this.userExists(ctx, userID);
        if (!exists) {
            throw new Error(`The user ${userID} does not exist`);
        }
 
        /*
        const exists2 = await this.entrepriseExists(ctx, type);
        if (!exists2) {
            throw new Error(`The entreprise ${entreprise} does not exist`);
        }      
        */

        // overwriting original user with new user

        const userJSON = await ctx.stub.getState(userID); // get the user from chaincode state
        const userOBJ = JSON.parse(userJSON);

        const updatedUser = {
            UserID: userID,
            DateUser: date,
            Type: userOBJ.Type,
            Entreprise: userOBJ.Entreprise,
            Password: password,
        };

        const userBuffer = Buffer.from(JSON.stringify(updatedUser));
        ctx.stub.setEvent('updatePasswordUser', userBuffer);

        await ctx.stub.putState(userID, userBuffer);

        console.info(`INFO YAM updatePasswordUser`);
        return JSON.stringify(updatedUser);
    }

    ////////////////////////////////////////////////////////////////////////////////

    // checkAvailability checks if asset stock is sufficient for the order  
    async checkAvailability(ctx, date, contratID, quantite) {
        const now = new Date();
        console.log('checkAvailability now: ' + now);

        const contratexists = await this.contratExists(ctx, contratID);
        if (!contratexists) {
            throw new Error(`The contrat ${contratID} does not exist`);
        }
        const contratJSON = await ctx.stub.getState(contratID); // get the contrat from chaincode state
        const contratOBJ = JSON.parse(contratJSON);
        const dateActuel = contratOBJ.Datecontrat;
        const quantityActuel = contratOBJ.Quantite;
        const userClientActuel = contratOBJ.UserClient;
        const entrepriseClientActuel = contratOBJ.EntrepriseClient;
        const id_marchandiseActuel = contratOBJ.Id_marchandise;
        const id_stockActuel = contratOBJ.Id_stock;
        const userFournisseurActuel = contratOBJ.UserFournisseur;
        const entrepriseFournisseurActuel = contratOBJ.EntrepriseFournisseur;

        console.log('quantityActuel: ' + quantityActuel);

        {
            //------------ transaction 1 -------------------------------------------------------------			
            // update contrat's statut to : checkAvailability            

            const contrat = {
                ContratID: contratID,
                Datecontrat: date,
                Statut: 'CHECK_AVAILABILITY',
                Quantite: quantite,
                UserClient: userClientActuel,
                EntrepriseClient: entrepriseClientActuel,
                Id_marchandise: id_marchandiseActuel,
                Id_stock: id_stockActuel,
                UserFournisseur: userFournisseurActuel,
                EntrepriseFournisseur: entrepriseFournisseurActuel
            };

            const stockContrat = Buffer.from(JSON.stringify(contrat));
            await ctx.stub.putState(contratID, stockContrat);
            console.info(`INFO YAM checkAvailability OK`);

            // raise an Event
            emitter.emit('event checkAvailability', stockContrat);

        }
        return ('Check if the quantity ' + quantite + ' is available for ' + id_marchandiseActuel);
    }

    ////////////////////////////////////////////////////////////////////////////////

    // proposalSupplier confirm if asset stock is sufficient for the order and gives the stock name
    async proposalSupplier(ctx, date, stockID, contratID, quantite, userfournisseur, entreprisefournisseur) {
        const now = new Date();
        console.log('proposalSupplier now: ' + now);
        const stockexists = await this.stockExists(ctx, stockID);
        if (!stockexists) {
            throw new Error(`The stock ${stockID} does not exist`);
        }
        const stockJSON = await ctx.stub.getState(stockID); // get the stock from chaincode state
        const stockOBJ = JSON.parse(stockJSON);
        const stockActuel = stockOBJ.Stock;
        console.log('stockActuel: ' + stockActuel);

        const contratexists = await this.contratExists(ctx, contratID);
        if (!contratexists) {
            throw new Error(`The contrat ${contratID} does not exist`);
        }
        const contratJSON = await ctx.stub.getState(contratID); // get the contrat from chaincode state
        const contratOBJ = JSON.parse(contratJSON);
        const dateActuel = contratOBJ.Datecontrat;
        const quantityActuel = contratOBJ.Quantite;
        const userClientActuel = contratOBJ.UserClient;
        const entrepriseClientActuel = contratOBJ.EntrepriseClient;
        const id_marchandiseActuel = contratOBJ.Id_marchandise;
        const id_stockActuel = contratOBJ.Id_stock;
        const userFournisseurActuel = contratOBJ.UserFournisseur;
        const entrepriseFournisseurActuel = contratOBJ.EntrepriseFournisseur;

        console.log('quantityActuel: ' + quantityActuel);

        // check if asset stock is sufficient for the order
        if (parseInt(stockActuel) < parseInt(quantite)) {
            console.info(`INFO YAM proposalSupplier NOT OK`);
            throw new Error(`Stock of ${stockID} is insufficient for the ordred`);
        } else {
            //------------ transaction 2 -------------------------------------------------------------			
            // update contrat's statut to : proposalSupplier

            const contrat = {
                ContratID: contratID,
                Datecontrat: date,
                Statut: 'PROPOSAL_SUPPLIER',
                Quantite: quantite,
                UserClient: userClientActuel,
                EntrepriseClient: entrepriseClientActuel,
                Id_marchandise: id_marchandiseActuel,
                Id_stock: stockID,
                UserFournisseur: userfournisseur,
                EntrepriseFournisseur: entreprisefournisseur
            };

            const stockContrat = Buffer.from(JSON.stringify(contrat));

            await ctx.stub.putState(contratID, stockContrat);
            console.info(`INFO YAM proposalSupplier OK`);

            // raise an Event
            emitter.emit('event proposalSupplier', stockContrat);

        }
        return ('The quantity ' + quantite + ' is proposed by ' + userfournisseur + ' in ' + stockID + ' for the client ' + userClientActuel + ' in ' + contratID);
    }

    ////////////////////////////////////////////////////////////////////////////////

    // validClient checks if asset stock is sufficient for the order  
    async validClient(ctx, date, contratID) {
        const now = new Date();
        console.log('validClient now: ' + now);

        const contratexists = await this.contratExists(ctx, contratID);
        if (!contratexists) {
            throw new Error(`The contrat ${contratID} does not exist`);
        }

        const contratJSON = await ctx.stub.getState(contratID); // get the contrat from chaincode state
        const contratOBJ = JSON.parse(contratJSON);
        const dateActuel = contratOBJ.Datecontrat;
        const quantityActuel = contratOBJ.Quantite;
        const userClientActuel = contratOBJ.UserClient;
        const entrepriseClientActuel = contratOBJ.EntrepriseClient;
        const id_marchandiseActuel = contratOBJ.Id_marchandise;
        const id_stockActuel = contratOBJ.Id_stock;
        const userFournisseurActuel = contratOBJ.UserFournisseur;
        const entrepriseFournisseurActuel = contratOBJ.EntrepriseFournisseur;

        console.log('quantityActuel: ' + quantityActuel);

        {
            //------------ transaction 3 -------------------------------------------------------------
            // update contrat's statut to : VALIDE_CLIENT

            const contrat = {
                ContratID: contratID,
                Datecontrat: date,
                Statut: 'VALIDE_CLIENT',
                Quantite: quantityActuel,
                UserClient: userClientActuel,
                EntrepriseClient: entrepriseClientActuel,
                Id_marchandise: id_marchandiseActuel,
                Id_stock: id_stockActuel,
                UserFournisseur: userFournisseurActuel,
                EntrepriseFournisseur: entrepriseFournisseurActuel
            };

            const stockContrat = Buffer.from(JSON.stringify(contrat));

            await ctx.stub.putState(contratID, stockContrat);
            console.info(`INFO YAM validClient OK`);

            // raise an Event
            emitter.emit('event validClient', stockContrat);

        }
        return ('The contrat ' + contratID + ' is validated by the client ' + userClientActuel + ' for the quantity ' +
            quantityActuel + ' of ' + id_marchandiseActuel + ' in ' + id_stockActuel);
    }

    ////////////////////////////////////////////////////////////////////////////////

    // progressDelivery updates stock and contrat    
    async progressDelivery(ctx, date, contratID) {
        const now = new Date();
        console.log('progressDelivery now: ' + now);

        const contratexists = await this.contratExists(ctx, contratID);
        if (!contratexists) {
            throw new Error(`The contrat ${contratID} does not exist`);
        }

        const contratJSON = await ctx.stub.getState(contratID); // get the contrat from chaincode state
        const contratOBJ = JSON.parse(contratJSON);
        const dateActuel = contratOBJ.Datecontrat;
        const quantityActuel = contratOBJ.Quantite;
        const userClientActuel = contratOBJ.UserClient;
        const entrepriseClientActuel = contratOBJ.EntrepriseClient;
        const id_marchandiseActuel = contratOBJ.Id_marchandise;
        const id_stockActuel = contratOBJ.Id_stock;
        const userFournisseurActuel = contratOBJ.UserFournisseur;
        const entrepriseFournisseurActuel = contratOBJ.EntrepriseFournisseur;

        console.log('quantityActuel: ' + quantityActuel);

        const stockexists = await this.stockExists(ctx, id_stockActuel);
        if (!stockexists) {
            throw new Error(`The stock ${id_stockActuel} does not exist`);
        }
        const stockJSON = await ctx.stub.getState(id_stockActuel); // get the stock from chaincode state
        const stockOBJ = JSON.parse(stockJSON);
        const stockActuel = stockOBJ.Stock;
        console.log('stockActuel: ' + stockActuel);

        // check if asset stock is sufficient for the order

        if (parseInt(stockActuel) < parseInt(quantityActuel)) {
            console.info(`INFO YAM progressDelivery NOT OK`);
            throw new Error(`Stock of ${id_stockActuel} is insufficient for the ordred`);
        } else {
            //------------ transaction 4 -------------------------------------------------------------
            // update contrat's statut to : LIVRAISON_EN_COURS

            const contrat = {
                ContratID: contratID,
                Datecontrat: date,
                Statut: 'LIVRAISON_EN_COURS',
                Quantite: quantityActuel,
                UserClient: userClientActuel,
                EntrepriseClient: entrepriseClientActuel,
                Id_marchandise: id_marchandiseActuel,
                Id_stock: id_stockActuel,
                UserFournisseur: userFournisseurActuel,
                EntrepriseFournisseur: entrepriseFournisseurActuel
            };

            const stockContrat = Buffer.from(JSON.stringify(contrat));

            await ctx.stub.putState(contratID, stockContrat);

            const Stoc = {
                StockID: id_stockActuel,
                Datestock: date,
                Stock: String((parseInt(stockActuel) - parseInt(quantityActuel))),
                EnLivraison: quantityActuel,
                UserFournisseur: userFournisseurActuel,
                EntrepriseFournisseur: entrepriseFournisseurActuel,
                Id_marchandise: id_marchandiseActuel,
                Id_contrat: contratID,
                UserClient: userClientActuel,
                EntrepriseClient: entrepriseClientActuel
            };

            const stockBuffer = Buffer.from(JSON.stringify(Stoc));
            ctx.stub.setEvent('progressDelivery', stockBuffer);

            await ctx.stub.putState(id_stockActuel, stockBuffer);

            console.info(`INFO YAM progressDelivery OK`);

            // raise an Event
            emitter.emit('event progressDelivery', stockContrat);

        }

        return ('Delivery is in progress for ' + contratID + ' by ' + userFournisseurActuel + ' for ' + userClientActuel + ' with the quantity ' +
            quantityActuel + ' of ' + id_marchandiseActuel + ' in ' + id_stockActuel);

    }

    ////////////////////////////////////////////////////////////////////////////////

    // finalizeDelivery updates stock and contrat    
    async finalizeDelivery(ctx, date, contratID) {
        const now = new Date();
        console.log('finalizeDelivery now: ' + now);

        const contratexists = await this.contratExists(ctx, contratID);
        if (!contratexists) {
            throw new Error(`The contrat ${contratID} does not exist`);
        }

        const contratJSON = await ctx.stub.getState(contratID); // get the contrat from chaincode state
        const contratOBJ = JSON.parse(contratJSON);
        const dateActuel = contratOBJ.Datecontrat;
        const quantityActuel = contratOBJ.Quantite;
        const userClientActuel = contratOBJ.UserClient;
        const entrepriseClientActuel = contratOBJ.EntrepriseClient;
        const id_marchandiseActuel = contratOBJ.Id_marchandise;
        const id_stockActuel = contratOBJ.Id_stock;
        const userFournisseurActuel = contratOBJ.UserFournisseur;
        const entrepriseFournisseurActuel = contratOBJ.EntrepriseFournisseur;

        console.log('quantityActuel: ' + quantityActuel);

        const stockexists = await this.stockExists(ctx, id_stockActuel);
        if (!stockexists) {
            throw new Error(`The stock ${id_stockActuel} does not exist`);
        }
        const stockJSON = await ctx.stub.getState(id_stockActuel); // get the stock from chaincode state
        const stockOBJ = JSON.parse(stockJSON);
        const stockActuel = stockOBJ.Stock;
        console.log('stockActuel: ' + stockActuel);

        {
            //------------ transaction 5 -------------------------------------------------------------
            // update contrat's statut to : LIVRAISON_TERMINEE

            const contrat = {
                ContratID: contratID,
                Datecontrat: date,
                Statut: 'LIVRAISON_TERMINEE',
                Quantite: quantityActuel,
                UserClient: userClientActuel,
                EntrepriseClient: entrepriseClientActuel,
                Id_marchandise: id_marchandiseActuel,
                Id_stock: id_stockActuel,
                UserFournisseur: userFournisseurActuel,
                EntrepriseFournisseur: entrepriseFournisseurActuel
            };

            const stockContrat = Buffer.from(JSON.stringify(contrat));

            await ctx.stub.putState(contratID, stockContrat);

            const Stoc = {
                StockID: id_stockActuel,
                Datestock: date,
                Stock: stockActuel,
                EnLivraison: "0",
                UserFournisseur: userFournisseurActuel,
                EntrepriseFournisseur: entrepriseFournisseurActuel,
                Id_marchandise: id_marchandiseActuel,
                Id_contrat: contratID,
                UserClient: userClientActuel,
                EntrepriseClient: entrepriseClientActuel
            };

            const stockBuffer = Buffer.from(JSON.stringify(Stoc));
            ctx.stub.setEvent('finalizeDelivery', stockBuffer);

            await ctx.stub.putState(id_stockActuel, stockBuffer);

            console.info(`INFO YAM finalizeDelivery OK`);

            // raise an Event
            emitter.emit('event finalizeDelivery', stockContrat);

        }

        return ('Delivery is terminating for ' + contratID + ' by ' + userFournisseurActuel + ' for ' + userClientActuel + ' with the quantity ' +
            quantityActuel + ' of ' + id_marchandiseActuel + ' in ' + id_stockActuel);

    }

    ////////////////////////////////////////////////////////////////////////////////

    // deleteStock deletes an given stock from the world state.
    async deleteStock(ctx, id) {
        const now = new Date();
        console.log('deleteStock now: ' + now);
        const exists = await this.stockExists(ctx, id);
        if (!exists) {
            throw new Error(`The stock ${id} does not exist`);
        }
        return ctx.stub.deleteState(id);
    }

    ////////////////////////////////////////////////////////////////////////////////

    // deleteContrat deletes an given contrat from the world state.
    async deleteContrat(ctx, id) {
        const now = new Date();
        console.log('deleteContrat now: ' + now);
        const exists = await this.contratExists(ctx, id);
        if (!exists) {
            throw new Error(`The contrat ${id} does not exist`);
        }
        return ctx.stub.deleteState(id);
    }

    ////////////////////////////////////////////////////////////////////////////////

    // deleteUser deletes an given user from the world state.
    async deleteUser(ctx, id) {
        const now = new Date();
        console.log('deleteUser now: ' + now);
        const exists = await this.userExists(ctx, id);
        if (!exists) {
            throw new Error(`The user ${id} does not exist`);
        }
        return ctx.stub.deleteState(id);
    }

    ////////////////////////////////////////////////////////////////////////////////

    // deleteEntreprise deletes an given entreprise from the world state.
    async deleteEntreprise(ctx, id) {
        const now = new Date();
        console.log('deleteEntreprise now: ' + now);
        const exists = await this.entrepriseExists(ctx, id);
        if (!exists) {
            throw new Error(`The entreprise ${id} does not exist`);
        }
        return ctx.stub.deleteState(id);
    }

    ////////////////////////////////////////////////////////////////////////////////

    // deleteMarchandise deletes an given marchandise from the world state.
    async deleteMarchandise(ctx, id) {
        const now = new Date();
        console.log('deleteMarchandise now: ' + now);
        const exists = await this.marchandiseExists(ctx, id);
        if (!exists) {
            throw new Error(`The marchandise ${id} does not exist`);
        }
        return ctx.stub.deleteState(id);
    }

    ////////////////////////////////////////////////////////////////////////////////

    // stockExists returns true when stock with given stockid exists in world state.
    async stockExists(ctx, stockid) {
        const stockJSON = await ctx.stub.getState(stockid);
        return stockJSON && stockJSON.length > 0;
    }

    ////////////////////////////////////////////////////////////////////////////////

    // contratExists returns true when contrat with given contratid exists in world state.
    async contratExists(ctx, contratid) {
        const contratJSON = await ctx.stub.getState(contratid);
        return contratJSON && contratJSON.length > 0;
    }

    ////////////////////////////////////////////////////////////////////////////////

    // userExists returns true when user with given userid exists in world state.
    async userExists(ctx, userid) {
        const userJSON = await ctx.stub.getState(userid);
        return userJSON && userJSON.length > 0;
    }

    ////////////////////////////////////////////////////////////////////////////////

    // entrepriseExists returns true when entreprise with given entrepriseid exists in world state.
    async entrepriseExists(ctx, entrepriseid) {
        const entrepriseJSON = await ctx.stub.getState(entrepriseid);
        return entrepriseJSON && entrepriseJSON.length > 0;
    }

    ////////////////////////////////////////////////////////////////////////////////

    // marchandiseExists returns true when marchandise with given marchandiseid exists in world state.
    async marchandiseExists(ctx, marchandiseid) {
        const marchandiseJSON = await ctx.stub.getState(marchandiseid);
        return marchandiseJSON && marchandiseJSON.length > 0;
    }

    ////////////////////////////////////////////////////////////////////////////////

    // AssetExists returns true when asset with given ID exists in world state.
    async AssetExists(ctx, id) {
        const assetJSON = await ctx.stub.getState(id);
        return assetJSON && assetJSON.length > 0;
    }

    // TransferAsset updates the owner field of asset with given id in the world state.
    async TransferAsset(ctx, id, newOwner) {
        const now = new Date();
        console.log('TransferAsset now: ' + now);
        const assetString = await this.ReadAsset(ctx, id);
        const asset = JSON.parse(assetString);
        asset.Owner = newOwner;
        return ctx.stub.putState(id, Buffer.from(JSON.stringify(asset)));
    }

    // getAllAssets returns all assets found in the world state.
    async getAllAssets(ctx) {
        const allResults = [];
        // range query with empty string for startKey and endKey does an open-ended query of all assets in the chaincode namespace.
        const iterator = await ctx.stub.getStateByRange('', '');
        let result = await iterator.next();
        let stockid;
        while (!result.done) {
            const strValue = Buffer.from(result.value.value.toString()).toString('utf8');
            let record;
            try {
                record = JSON.parse(strValue);
                stockid = record.StockID;
                console.log('StockID: ' + stockid);
            } catch (err) {
                console.log(err);
                record = strValue;
            }

            if (stockid !== undefined) {
                allResults.push({ Key: result.value.key, Record: record });
            }

            result = await iterator.next();
        }
        return JSON.stringify(allResults);
    }

    ////////////////////////////////////////////////////////////////////////////////

    // getAllStocks returns all stocks found in the world state.
    async getAllStocks(ctx) {
        const allResults = [];
        // range query with empty string for startKey and endKey does an open-ended query of all assets in the chaincode namespace.
        const iterator = await ctx.stub.getStateByRange('', '');
        let result = await iterator.next();
        let stockid;
        while (!result.done) {
            const strValue = Buffer.from(result.value.value.toString()).toString('utf8');
            let record;
            try {
                record = JSON.parse(strValue);
                stockid = record.StockID;
                console.log('StockID: ' + stockid);
            } catch (err) {
                console.log(err);
                record = strValue;
            }

            if (stockid !== undefined) {

                //allResults.push({ Key: result.value.key, Record: record });

                allResults.push(record);
            }

            result = await iterator.next();
        }
        return JSON.stringify(allResults);
    }

    ////////////////////////////////////////////////////////////////////////////////

    // getAllContrats returns all contrats found in the world state.
    async getAllContrats(ctx) {
        const allResults = [];
        // range query with empty string for startKey and endKey does an open-ended query of all assets in the chaincode namespace.
        const iterator = await ctx.stub.getStateByRange('', '');
        let result = await iterator.next();
        let contratID;

        while (!result.done) {
            const strValue = Buffer.from(result.value.value.toString()).toString('utf8');
            let record;
            try {
                record = JSON.parse(strValue);
                contratID = record.ContratID;
                console.log('ContratID: ' + contratID);
            } catch (err) {
                console.log(err);
                record = strValue;
            }

            if (contratID !== undefined) {
                //allResults.push({ Key: result.value.key, Record: record });
                allResults.push(record);

            }

            result = await iterator.next();
        }
        return JSON.stringify(allResults);
    }

    ////////////////////////////////////////////////////////////////////////////////

    // getAllUsers returns all users found in the world state.
    async getAllUsers(ctx) {
        const allResults = [];
        // range query with empty string for startKey and endKey does an open-ended query of all assets in the chaincode namespace.
        const iterator = await ctx.stub.getStateByRange('', '');
        let result = await iterator.next();
        let userid;
        while (!result.done) {
            const strValue = Buffer.from(result.value.value.toString()).toString('utf8');
            let record;
            try {
                record = JSON.parse(strValue);
                userid = record.UserID;
                console.log('UserID: ' + userid);
            } catch (err) {
                console.log(err);
                record = strValue;
            }

            if (userid !== undefined) {

                //allResults.push({ Key: result.value.key, Record: record });

                allResults.push(record);
            }

            result = await iterator.next();
        }
        return JSON.stringify(allResults);
    }

    ////////////////////////////////////////////////////////////////////////////////

    // getAllEntreprises returns all entreprise found in the world state.
    async getAllEntreprises(ctx) {
        const allResults = [];
        // range query with empty string for startKey and endKey does an open-ended query of all assets in the chaincode namespace.
        const iterator = await ctx.stub.getStateByRange('', '');
        let result = await iterator.next();
        let entrepriseid;
        while (!result.done) {
            const strValue = Buffer.from(result.value.value.toString()).toString('utf8');
            let record;
            try {
                record = JSON.parse(strValue);
                entrepriseid = record.EntrepriseID;
                console.log('EntrepriseID: ' + entrepriseid);
            } catch (err) {
                console.log(err);
                record = strValue;
            }

            if (entrepriseid !== undefined) {

                //allResults.push({ Key: result.value.key, Record: record });

                allResults.push(record);
            }

            result = await iterator.next();
        }
        return JSON.stringify(allResults);
    }

    ////////////////////////////////////////////////////////////////////////////////

    // getAllMarchandises returns all marchandise found in the world state.
    async getAllMarchandises(ctx) {
        const allResults = [];
        // range query with empty string for startKey and endKey does an open-ended query of all assets in the chaincode namespace.
        const iterator = await ctx.stub.getStateByRange('', '');
        let result = await iterator.next();
        let marchandiseid;
        while (!result.done) {
            const strValue = Buffer.from(result.value.value.toString()).toString('utf8');
            let record;
            try {
                record = JSON.parse(strValue);
                marchandiseid = record.MarchandiseID;
                console.log('MarchandiseID: ' + marchandiseid);
            } catch (err) {
                console.log(err);
                record = strValue;
            }

            if (marchandiseid !== undefined) {

                //allResults.push({ Key: result.value.key, Record: record });

                allResults.push(record);
            }

            result = await iterator.next();
        }
        return JSON.stringify(allResults);
    }

    ////////////////////////////////////////////////////////////////////////////////

    // getAllUsersByEntreprise returns all users by entrprise found in the world state.
    async getAllUsersByEntreprise(ctx, entreprise) {
        const allResults = [];
        // range query with empty string for startKey and endKey does an open-ended query of all assets in the chaincode namespace.
        const iterator = await ctx.stub.getStateByRange('', '');
        let result = await iterator.next();
        let userID;
        let Entreprise;

        while (!result.done) {
            const strValue = Buffer.from(result.value.value.toString()).toString('utf8');
            let record;
            try {
                record = JSON.parse(strValue);
                userID = record.UserID;
                Entreprise = record.Entreprise;
                console.log('UserID: ' + userID + ', Entreprise: ' + Entreprise);
            } catch (err) {
                console.log(err);
                record = strValue;
            }
            if (userID !== undefined) {
                if (Entreprise == entreprise) {
                    allResults.push({ Key: result.value.key, Record: record });
                }
            }
            result = await iterator.next();
        }
        return JSON.stringify(allResults);
    }

    ////////////////////////////////////////////////////////////////////////////////

    // getAllUsersByType returns all users by type found in the world state.
    async getAllUsersByType(ctx, type) {
        const allResults = [];
        // range query with empty string for startKey and endKey does an open-ended query of all assets in the chaincode namespace.
        const iterator = await ctx.stub.getStateByRange('', '');
        let result = await iterator.next();
        let userID;
        let Type;

        while (!result.done) {
            const strValue = Buffer.from(result.value.value.toString()).toString('utf8');
            let record;
            try {
                record = JSON.parse(strValue);
                userID = record.UserID;
                Type = record.Type;
                console.log('UserID: ' + userID + ', Type: ' + Type);
            } catch (err) {
                console.log(err);
                record = strValue;
            }
            if (userID !== undefined) {
                if (Type == type) {
                    allResults.push({ Key: result.value.key, Record: record });
                }
            }
            result = await iterator.next();
        }
        return JSON.stringify(allResults);
    }

    ////////////////////////////////////////////////////////////////////////////////

    // getAllContratsByStatus returns all contrats by status found in the world state.
    async getAllContratsByStatus(ctx, contratstatus) {
        const allResults = [];
        // range query with empty string for startKey and endKey does an open-ended query of all assets in the chaincode namespace.
        const iterator = await ctx.stub.getStateByRange('', '');
        let result = await iterator.next();
        let contratID;
        let statusContrat;

        while (!result.done) {
            const strValue = Buffer.from(result.value.value.toString()).toString('utf8');
            let record;
            try {
                record = JSON.parse(strValue);
                contratID = record.ContratID;
                statusContrat = record.Statut;
                console.log('ContratID: ' + contratID + ', Status: ' + statusContrat);
            } catch (err) {
                console.log(err);
                record = strValue;
            }
            if (contratID !== undefined) {
                if (statusContrat == contratstatus) {
                    allResults.push({ Key: result.value.key, Record: record });
                }
            }
            result = await iterator.next();
        }
        return JSON.stringify(allResults);
    }

    ////////////////////////////////////////////////////////////////////////////////	

    // getAllContratsByDate returns all contrats by date found in the world state.	

    async getAllContratsByDate(ctx, startDate, endDate) {
        const allResults = [];
        // range query with empty string for startKey and endKey does an open-ended query of all assets in the chaincode namespace.
        const iterator = await ctx.stub.getStateByRange('', '');
        let result = await iterator.next();
        let contratID;
        let dateContrat;

        while (!result.done) {
            const strValue = Buffer.from(result.value.value.toString()).toString('utf8');
            let record;
            try {
                record = JSON.parse(strValue);
                contratID = record.ContratID;
                dateContrat = record.Datecontrat;
                console.log('ContratID: ' + contratID + ', Date: ' + dateContrat);
            } catch (err) {
                console.log(err);
                record = strValue;
            }
            if (contratID !== undefined) {
                if ((startDate < dateContrat) && (dateContrat < endDate)) {
                    allResults.push({ Key: result.value.key, Record: record });
                }
            }
            result = await iterator.next();
        }
        return JSON.stringify(allResults);
    }

    ////////////////////////////////////////////////////////////////////////////////	




}

module.exports = AssetTransfer;
