/*
 * Copyright IBM Corp. All Rights Reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const {Contract} = require('fabric-contract-api');

//const Client = require('fabric-client');
const fs = require("fs");

//const State = require('../ledger-api/state.js');

//////////////////////////////////////////////////////////////////////////////////////////////
// Events management

const EventEmitter = require('events');

const emitter = new EventEmitter();

// register all listeners
/*
emitter.on('event createStock', (info) => {console.info('Event Listener createStock called : ' + info);});	
emitter.on('event createContrat', (info) => {console.info('Event Listener createContrat called : ' + info);});	
emitter.on('event checkAvailability', (info) => {console.info('Event Listener checkAvailability called : ' + info);});	
emitter.on('event validClient', (info) => {console.info('Event Listener validClient called : ' + info);});	
emitter.on('event progressDelivery', (info) => {console.info('Event Listener progressDelivery called : ' + info);});	
emitter.on('event finalizeDelivery', (info) => {console.info('Event Listener finalizeDelivery called : ' + info);});	
*/
/*
checkAvailability","Args":["1649855289","stockYAM1","contratYAM1","30"]}'
validClient","Args":["1649855289","stockYAM1","contratYAM1","30"]}'
progressDelivery","Args":["1649855289","stockYAM1","contratYAM1","30"]}'
finalizeDelivery","Args":["1649855289","stockYAM1","contratYAM1","30"]}'
*/

//////////////////////////////////////////////////////////////////////////////////////////////


class AssetTransfer extends Contract {

    async InitLedger(ctx) {
        const assets = [
            {
                ID: 'asset1',
                Color: 'blue',
                Size: 5,
                Owner: 'Tomoko',
                AppraisedValue: 300,
            },
            {
                ID: 'asset2',
                Color: 'red',
                Size: 5,
                Owner: 'Brad',
                AppraisedValue: 400,
            },
            {
                ID: 'asset3',
                Color: 'green',
                Size: 10,
                Owner: 'Jin Soo',
                AppraisedValue: 500,
            },
            {
                ID: 'asset4',
                Color: 'yellow',
                Size: 10,
                Owner: 'Max',
                AppraisedValue: 600,
            },
            {
                ID: 'asset5',
                Color: 'black',
                Size: 15,
                Owner: 'Adriana',
                AppraisedValue: 700,
            },
            {
                ID: 'asset6',
                Color: 'white',
                Size: 15,
                Owner: 'Michel',
                AppraisedValue: 800,
            },
        ];

        for (const asset of assets) {
            asset.docType = 'asset';
            await ctx.stub.putState(asset.ID, Buffer.from(JSON.stringify(asset)));
            console.info(`Asset ${asset.ID} initialized`);
        }
    }
//////////////////////////////////////////////////////////////////////////////////////////////

    async getHistory(ctx, key) {
				
    const promiseOfIterator = ctx.stub.getHistoryForKey(key);

    const results = [];
    for await (const keyMod of promiseOfIterator) {
    console.log('keymod: '+JSON.stringify(keyMod));
    const ts =  keyMod.timestamp;
    const milliseconds = (ts.seconds.low + ((ts.nanos / 1000000) / 1000)) * 1000;
    const date =  new Date(milliseconds);	
    console.log('timestamp: '+ date);	
    const transid = keyMod.txId;
    console.log('txId: '+ transid);
    const val = keyMod.value.toString('utf8');
    console.log('value: '+ val);	
		
//    const del =  keyMod.is_delete;
//    console.log('del: '+ del);

    const resp = {
//        timestamp:date,			
        timestamp:keyMod.timestamp,		
        txid: transid,
		value : val
    }
    results.push(resp);
    }
// results array contains the key history
    return results;  	
    }

//////////////////////////////////////////////////////////////////////////////////////////////

    async getEnv(ctx, key) {
			
	const bind = ctx.stub.getBinding(key);
    console.log('getBinding: '+ bind);	
	
	const creat = ctx.stub.getCreator(key);
    console.log('getCreator: '+ JSON.stringify(creat));	
	
	const msp = ctx.stub.getMspID(key);
    console.log('getMspID: '+ msp);	
	
	const trans = ctx.stub.getTransient(key);
    console.log('getTransient: '+ JSON.stringify(trans));	
	
	const txid = ctx.stub.getTxID(key);
    console.log('getTxID: '+ txid);	

    const resp = {
	    Transient : trans,
	    Txid : txid,		
	    Binding : bind,
	    Creator : creat
//	    Msp : msp,

    }

    return resp;  	
    }


//////////////////////////////////////////////////////////////////////////////////////////////

    // createStock issues a new stock to the world state with given details.
    async createStock(ctx, date, stockID, stock, enLivraison) {	
		
        const exists = await this.stockExists(ctx, stockID);
        if (exists) {
            throw new Error(`The stock ${stockID} already exists`);
        }		
//        const now = await this.GetTxTimestamp();				
        const now = new Date();				
        console.log('createStock now: '+ now);	
//        const milliseconds = (date.seconds.low + ((date.nanos / 1000000) / 1000)) * 1000;
        const milliseconds = (date) * 1000;
        const dateconv =  new Date(milliseconds);	
        console.log('dateconv: '+ dateconv);				
        const Stoc = {
            StockID: stockID,
            Datestock : dateconv,						     
//			Datestock : now,						
            Stock: stock,
            EnLivraison: enLivraison,
        };			

		const stockBuffer = Buffer.from(JSON.stringify(Stoc));		
		ctx.stub.setEvent('createStock', stockBuffer);
		
        await ctx.stub.putState(stockID, stockBuffer);
        console.info(`INFO YAM createStock`);
		
        // raise an Event
        emitter.emit('event createStock', stockBuffer );				
		
		
/*		
		try {			
        await fs.promises.writeFile(`/YAM.txt`, `SALUT createStock`);
        } catch (err) {
        console.error(`Erreur ecriture createStock`);
        } 
		

        // check to see if there is a config json defined
        if (fs.existsSync(addAssetsConfigFile)) {
            // read file the next asset and number of assets to create
            let addAssetsConfigJSON = fs.readFileSync(addAssetsConfigFile, 'utf8');
            addAssetsConfig = JSON.parse(addAssetsConfigJSON);
            nextAssetNumber = addAssetsConfig.nextAssetNumber;
            numberAssetsToAdd = addAssetsConfig.numberAssetsToAdd;
        } else {
            nextAssetNumber = 100;
            numberAssetsToAdd = 20;
            // create a default config and save
            addAssetsConfig = new Object;
            addAssetsConfig.nextAssetNumber = nextAssetNumber;
            addAssetsConfig.numberAssetsToAdd = numberAssetsToAdd;
            fs.writeFileSync(addAssetsConfigFile, JSON.stringify(addAssetsConfig, null, 2));
        }
		
		
		
		const addAssetsConfigFile = path.resolve(__dirname, 'addAssets.json');
		
        fs.writeFileSync(addAssetsConfigFile, JSON.stringify(addAssetsConfig, null, 2));		
		
*/		
        return JSON.stringify(Stoc);
    }

//////////////////////////////////////////////////////////////////////////////////////////////
	
    // createContrat issues a new contrat to the world state with given details.
    async createContrat(ctx, date, contratID, statut, quantite) {	
		
        const exists = await this.contratExists(ctx, contratID);
        if (exists) {
            throw new Error(`The contrat ${contratID} already exists`);
        }
//        const now = await this.GetTxTimestamp();		
        const now = new Date();		
        console.log('createContrat now: '+ now);
//        const milliseconds = (date.seconds.low + ((date.nanos / 1000000) / 1000)) * 1000;
        const milliseconds = (date) * 1000;		
        const dateconv =  new Date(milliseconds);	
        console.log('dateconv: '+ dateconv);				
        const contrat = {
            ContratID: contratID,
			Datecontrat : dateconv,			
            Statut: statut,
            Quantite: quantite,
        };		
		
		const stockContrat = Buffer.from(JSON.stringify(contrat));				
		
        await ctx.stub.putState(contratID, stockContrat);
        console.info(`INFO YAM createContrat`);
				
        // raise an Event
        emitter.emit('event createContrat', stockContrat );				
		
/*			
		try {			
        await fs.promises.writeFile(`/YAM.txt`, `SALUT createContrat`);
        } catch (err) {
        console.error(`Erreur ecriture createContrat`);
        } 
*/		
        return JSON.stringify(contrat);
    }

//////////////////////////////////////////////////////////////////////////////////////////////

    // CreateAsset issues a new asset to the world state with given details.
    async CreateAsset(ctx, id, color, size, owner, appraisedValue) {
			
        const exists = await this.AssetExists(ctx, id);
        if (exists) {
            throw new Error(`The asset ${id} already exists`);
        }

        const asset = {
            ID: id,
            Color: color,
            Size: size,
            Owner: owner,
            AppraisedValue: appraisedValue,
        };
		
        await ctx.stub.putState(id, Buffer.from(JSON.stringify(asset)));
        console.info(`INFO YAM CreateAsset`);
		
		try {			
        await fs.promises.writeFile(`/YAM.txt`, `SALUT CreateAsset`);
        } catch (err) {
        console.error(`Erreur ecriture`);
        } 
		
        return JSON.stringify(asset);
    }

//////////////////////////////////////////////////////////////////////////////////////////////

    // readStock returns the stock stored in the world state with given id.
    async readStock(ctx, id) {
        const now = new Date();				
        console.log('readStock now: '+ now);		
        const stockJSON = await ctx.stub.getState(id); // get the stock from chaincode state
        if (!stockJSON || stockJSON.length === 0) {
            throw new Error(`The stock ${id} does not exist`);
        }
        console.log('stockJSON: '+ stockJSON.toString());		
        return stockJSON.toString();
    }

//////////////////////////////////////////////////////////////////////////////////////////////

    // readContrat returns the contrat stored in the world state with given id.
    async readContrat(ctx, id) {
        const now = new Date();				
        console.log('readContrat now: '+ now);		
        const contratJSON = await ctx.stub.getState(id); // get the contrat from chaincode state
        if (!contratJSON || contratJSON.length === 0) {
            throw new Error(`The contrat ${id} does not exist`);
        }
        console.log('contratJSON: '+ contratJSON.toString());		
        return contratJSON.toString();
    }

//////////////////////////////////////////////////////////////////////////////////////////////

    // getContratStatus returns the contrat status stored in the world state with given id.
    async getContratStatus(ctx, id) {
        const now = new Date();				
        console.log('getContratStatus now: '+ now);			
        const contratJSON = await ctx.stub.getState(id); // get the contrat from chaincode state
        if (!contratJSON || contratJSON.length === 0) {
            throw new Error(`The contrat ${id} does not exist`);
        }		
        const contratOBJ = JSON.parse(contratJSON);
		const statutActuel = contratOBJ.Statut;
        console.log('statutActuel: '+ statutActuel);		
        return statutActuel;

    }

//////////////////////////////////////////////////////////////////////////////////////////////

    // getContratQuantity returns the contrat quantity stored in the world state with given id.
    async getContratQuantity(ctx, id) {
        const now = new Date();				
        console.log('getContratQuantity now: '+ now);			
        const contratJSON = await ctx.stub.getState(id); // get the contrat from chaincode state
        if (!contratJSON || contratJSON.length === 0) {
            throw new Error(`The contrat ${id} does not exist`);
        }		
        const contratOBJ = JSON.parse(contratJSON);
		const quantityActuel = contratOBJ.Quantite;
        console.log('quantityActuel: '+ quantityActuel);		
        return quantityActuel;
    }

//////////////////////////////////////////////////////////////////////////////////////////////

    // getAssetStock returns the stock  Stock stored in the world state with given id.
    async getAssetStock(ctx, id) {
        const now = new Date();				
        console.log('getAssetStock now: '+ now);		
        const stockJSON = await ctx.stub.getState(id); // get the stock from chaincode state
        if (!stockJSON || stockJSON.length === 0) {
            throw new Error(`The stock ${id} does not exist`);
        }		
        const stockOBJ = JSON.parse(stockJSON);
		const stockActuel = stockOBJ.Stock;
        console.log('stockActuel: '+ stockActuel);		
        return stockActuel;
    }

//////////////////////////////////////////////////////////////////////////////////////////////

    // getAssetEnLivraison returns the stock  EnLivraison stored in the world state with given id.
    async getAssetEnLivraison(ctx, id) {
        const now = new Date();				
        console.log('getAssetEnLivraison now: '+ now);		
        const stockJSON = await ctx.stub.getState(id); // get the stock from chaincode state
        if (!stockJSON || stockJSON.length === 0) {
            throw new Error(`The stock ${id} does not exist`);
        }		
        const stockOBJ = JSON.parse(stockJSON);
		const enlivraisonActuel = stockOBJ.EnLivraison;
        console.log('enlivraisonActuel: '+ enlivraisonActuel);		
        return enlivraisonActuel;
    }

//////////////////////////////////////////////////////////////////////////////////////////////

    // ReadAsset returns the asset stored in the world state with given id.
    async ReadAsset(ctx, id) {
        const assetJSON = await ctx.stub.getState(id); // get the asset from chaincode state
        if (!assetJSON || assetJSON.length === 0) {
            throw new Error(`The asset ${id} does not exist`);
        }
        return assetJSON.toString();
    }

//////////////////////////////////////////////////////////////////////////////////////////////
   /**
     * augmentAssetStock.
     *
     * @param ctx the transaction context
     * @param stockID the ID of the stock
     * @param stockToadd the quantity to add
     * @return the stock found on the ledger if there was one
     */	
	
    // augmentAssetStock updates an existing stock in the world state with provided parameters.
    // ctx the transaction context
    // stockID the ID of the stock
    // stockToadd the quantity to add	
	
	
    async augmentAssetStock(ctx,date, stockID, stockToadd) {
        const now = new Date();				
        console.log('augmentAssetStock now: '+ now);			
        const exists = await this.stockExists(ctx, stockID);
        if (!exists) {
            throw new Error(`The stock ${stockID} does not exist`);
        }
        const stockJSON = await ctx.stub.getState(stockID); // get the stock from chaincode state
        const stockOBJ = JSON.parse(stockJSON);
		const stockActuel = stockOBJ.Stock;
        console.log('stockActuel: '+ stockActuel);
			
        const updatedStock = {
            StockID: stockID,
			Datestock : date,												
            Stock: String(parseInt(stockActuel)+parseInt(stockToadd)),
            EnLivraison: stockOBJ.EnLivraison,
        };		
				
        await ctx.stub.putState(stockID, Buffer.from(JSON.stringify(updatedStock)));
        console.info(`INFO YAM augmentAssetStock`);		
		
        return JSON.stringify(updatedStock);				
		
    }		
	
//////////////////////////////////////////////////////////////////////////////////////////////

    // UpdateAsset updates an existing asset in the world state with provided parameters.
    async UpdateAsset(ctx, id, color, size, owner, appraisedValue) {
        const exists = await this.AssetExists(ctx, id);
        if (!exists) {
            throw new Error(`The asset ${id} does not exist`);
        }

        // overwriting original asset with new asset
        const updatedAsset = {
            ID: id,
            Color: color,
            Size: size,
            Owner: owner,
            AppraisedValue: appraisedValue,
        };			
		
        return ctx.stub.putState(id, Buffer.from(JSON.stringify(updatedAsset)));
    }

    // deleteAsset deletes an given asset from the world state.
    async deleteAsset(ctx, id) {
        const exists = await this.AssetExists(ctx, id);
        if (!exists) {
            throw new Error(`The asset ${id} does not exist`);
        }
        return ctx.stub.deleteState(id);
    }
////////////////////////////////////////////////////////////////////////////////

    // updateStock updates an existing stock in the world state with provided parameters.
    async updateStock(ctx,stockID, stock, enLivraison) {
        const now = new Date();				
        console.log('updateStock now: '+ now);		
        const exists = await this.stockExists(ctx, stockID);
        if (!exists) {
            throw new Error(`The stock ${stockID} does not exist`);
        }
        // overwriting original stock with new stock
        const updatedStock = {
            StockID: stockID,
//			Datestock : date,									
            Stock: stock,
            EnLivraison: enLivraison,
        };				
        await ctx.stub.putState(stockID, Buffer.from(JSON.stringify(updatedStock)));
        console.info(`INFO YAM updateStock`);				
        return JSON.stringify(updatedStock);			
    }	

////////////////////////////////////////////////////////////////////////////////

    // checkAvailability checks if asset stock is sufficient for the order  
    async checkAvailability(ctx,date,stockID, contratID, quantite) {
        const now = new Date();				
        console.log('checkAvailability now: '+ now);			
        const stockexists = await this.stockExists(ctx, stockID);
        if (!stockexists) {
            throw new Error(`The stock ${stockID} does not exist`);
        }
        const stockJSON = await ctx.stub.getState(stockID); // get the stock from chaincode state
        const stockOBJ = JSON.parse(stockJSON);
		const stockActuel = stockOBJ.Stock;
        console.log('stockActuel: '+ stockActuel);
        const contratexists = await this.contratExists(ctx, contratID);
        if (!contratexists) {
            throw new Error(`The contrat ${contratID} does not exist`);
        }
        const contratJSON = await ctx.stub.getState(contratID); // get the contrat from chaincode state
        const contratOBJ = JSON.parse(contratJSON);
		const dateActuel = contratOBJ.Datecontrat;	
		const quantityActuel = contratOBJ.Quantite;
        console.log('quantityActuel: '+ quantityActuel);
         // check if asset stock is sufficient for the order
         if (parseInt(stockActuel) < parseInt(quantite)) {
            console.info(`INFO YAM checkAvailability NOT OK`); 			 			 
            throw new Error(`Stock of ${stockID} is insufficient for the ordred`);
        } else {
         //------------ transaction 1 -------------------------------------------------------------			
         // update contrat's statut to : VERIF_DISPO
        const contrat = {
            ContratID: contratID,
			Datecontrat : date,
            Statut: 'VERIF_DISPO',
            Quantite: quantityActuel,
        };		
		
		const stockContrat = Buffer.from(JSON.stringify(contrat));						
			
        await ctx.stub.putState(contratID, stockContrat);
        console.info(`INFO YAM checkAvailability OK`); 
	
        // raise an Event
        emitter.emit('event checkAvailability', stockContrat );
		
        }				
        return ('The quantity of '+quantite+ ' is available for the ordred' );		
    }	
	

////////////////////////////////////////////////////////////////////////////////

    // validClient checks if asset stock is sufficient for the order  
    async validClient(ctx,date,stockID, contratID, quantite) {
        const now = new Date();				
        console.log('validClient now: '+ now);		
        const stockexists = await this.stockExists(ctx, stockID);
        if (!stockexists) {
            throw new Error(`The stock ${stockID} does not exist`);
        }
        const stockJSON = await ctx.stub.getState(stockID); // get the stock from chaincode state
        const stockOBJ = JSON.parse(stockJSON);
		const stockActuel = stockOBJ.Stock;
        console.log('stockActuel: '+ stockActuel);
        const contratexists = await this.contratExists(ctx, contratID);
        if (!contratexists) {
            throw new Error(`The contrat ${contratID} does not exist`);
        }
        const contratJSON = await ctx.stub.getState(contratID); // get the contrat from chaincode state
        const contratOBJ = JSON.parse(contratJSON);
		const dateActuel = contratOBJ.Datecontrat;
		const quantityActuel = contratOBJ.Quantite;
        console.log('quantityActuel: '+ quantityActuel);
         // check if asset stock is sufficient for the order
         if (parseInt(stockActuel) < parseInt(quantite)) {
            console.info(`INFO YAM validClient NOT OK`); 			 			 
            throw new Error(`Stock of ${stockID} is insufficient for the ordred`);
        } else {
         //------------ transaction 2 -------------------------------------------------------------
         // update contrat's statut to : VALIDE_CLIENT
        const contrat = {
            ContratID: contratID,
			Datecontrat : date,
            Statut: 'VALIDE_CLIENT',
            Quantite: quantityActuel,
        };		
				
		const stockContrat = Buffer.from(JSON.stringify(contrat));						
			
        await ctx.stub.putState(contratID, stockContrat);
        console.info(`INFO YAM validClient OK`); 
	
        // raise an Event
        emitter.emit('event validClient', stockContrat );
			 
        }				
        return ('The quantity of '+quantite+ ' is validated for the ordred' );		
    }	
	

////////////////////////////////////////////////////////////////////////////////

    // progressDelivery updates stock and contrat    
    async progressDelivery(ctx,date,stockID, contratID, quantite) {
        const now = new Date();				
        console.log('progressDelivery now: '+ now);			
        const stockexists = await this.stockExists(ctx, stockID);
        if (!stockexists) {
            throw new Error(`The stock ${stockID} does not exist`);
        }
        const stockJSON = await ctx.stub.getState(stockID); // get the stock from chaincode state
        const stockOBJ = JSON.parse(stockJSON);
		const stockActuel = stockOBJ.Stock;
        console.log('stockActuel: '+ stockActuel);
        const contratexists = await this.contratExists(ctx, contratID);
        if (!contratexists) {
            throw new Error(`The contrat ${contratID} does not exist`);
        }
        const contratJSON = await ctx.stub.getState(contratID); // get the contrat from chaincode state
        const contratOBJ = JSON.parse(contratJSON);
		const dateActuel = contratOBJ.Datecontrat;	
		const quantityActuel = contratOBJ.Quantite;
        console.log('quantityActuel: '+ quantityActuel);
         // check if asset stock is sufficient for the order
         if (parseInt(stockActuel) < parseInt(quantite)) {
            console.info(`INFO YAM progressDelivery NOT OK`); 			 			 
            throw new Error(`Stock of ${stockID} is insufficient for the ordred`);
        } else {
         //------------ transaction 3 -------------------------------------------------------------
         // update contrat's statut to : LIVRAISON_EN_COURS
        const contrat = {
            ContratID: contratID,
			Datecontrat : date,
            Statut: 'LIVRAISON_EN_COURS',
            Quantite: quantite,
        };		
						
		const stockContrat = Buffer.from(JSON.stringify(contrat));						
			
        await ctx.stub.putState(contratID, stockContrat);
					
        // updating original stock  
        const updatedStock = {
            StockID: stockID,
			Datestock : date,									
            Stock: String((parseInt(stockActuel) - parseInt(quantite))),
            EnLivraison: quantite,
        };			
        await ctx.stub.putState(stockID, Buffer.from(JSON.stringify(updatedStock)));
		
        console.info(`INFO YAM progressDelivery OK`); 

        // raise an Event
        emitter.emit('event progressDelivery', stockContrat );
		
        }				
        return ('delivery in progress of '+quantite );			
    }	
	
////////////////////////////////////////////////////////////////////////////////

    // finalizeDelivery updates stock and contrat  
    async finalizeDelivery(ctx,date,stockID, contratID, quantite) {
        const now = new Date();				
        console.log('finalizeDelivery now: '+ now);			
        const stockexists = await this.stockExists(ctx, stockID);
        if (!stockexists) {
            throw new Error(`The stock ${stockID} does not exist`);
        }
        const stockJSON = await ctx.stub.getState(stockID); // get the stock from chaincode state
        const stockOBJ = JSON.parse(stockJSON);
		const stockActuel = stockOBJ.Stock;
        console.log('stockActuel: '+ stockActuel);
        const contratexists = await this.contratExists(ctx, contratID);
        if (!contratexists) {
            throw new Error(`The contrat ${contratID} does not exist`);
        }
        const contratJSON = await ctx.stub.getState(contratID); // get the contrat from chaincode state
        const contratOBJ = JSON.parse(contratJSON);
		const dateActuel = contratOBJ.Datecontrat;		
		const quantityActuel = contratOBJ.Quantite;
        console.log('quantityActuel: '+ quantityActuel);
         //------------ transaction 4 -------------------------------------------------------------
         // update contrat's statut to : LIVRAISON_TERMINEE
        const contrat = {
            ContratID: contratID,
			Datecontrat : date,						
			Datecontrat: dateActuel,
            Statut: 'LIVRAISON_TERMINEE',
            Quantite: quantite,
        };		
						
		const stockContrat = Buffer.from(JSON.stringify(contrat));						
			
        await ctx.stub.putState(contratID, stockContrat);
		
        // updating original stock  
        const updatedStock = {
            StockID: stockID,
			Datestock : date,									
            Stock: stockActuel,
            EnLivraison: "0",
        };			
        await ctx.stub.putState(stockID, Buffer.from(JSON.stringify(updatedStock)));
		
        console.info(`INFO YAM finalizeDelivery OK`); 

        // raise an Event
        emitter.emit('event finalizeDelivery', stockContrat );
				
        return ('delivery completed of '+quantite );			
    }	
	
////////////////////////////////////////////////////////////////////////////////

   // deleteStock deletes an given stock from the world state.
    async deleteStock(ctx, id) {
        const now = new Date();				
        console.log('deleteStock now: '+ now);			
        const exists = await this.stockExists(ctx, id);
        if (!exists) {
            throw new Error(`The stock ${id} does not exist`);
        }
        return ctx.stub.deleteState(id);
    }	

////////////////////////////////////////////////////////////////////////////////
	
   // deleteContrat deletes an given contrat from the world state.
    async deleteContrat(ctx, id) {
        const now = new Date();				
        console.log('deleteContrat now: '+ now);			
        const exists = await this.contratExists(ctx, id);
        if (!exists) {
            throw new Error(`The contrat ${id} does not exist`);
        }
        return ctx.stub.deleteState(id);
    }	

////////////////////////////////////////////////////////////////////////////////

    // stockExists returns true when stock with given stockid exists in world state.
    async stockExists(ctx, stockid) {
        const stockJSON = await ctx.stub.getState(stockid);
        return stockJSON && stockJSON.length > 0;
    }

////////////////////////////////////////////////////////////////////////////////

    // contratExists returns true when contrat with given contratid exists in world state.
    async contratExists(ctx, contratid) {
        const contratJSON = await ctx.stub.getState(contratid);
        return contratJSON && contratJSON.length > 0;
    }

////////////////////////////////////////////////////////////////////////////////

    // AssetExists returns true when asset with given ID exists in world state.
    async AssetExists(ctx, id) {
        const assetJSON = await ctx.stub.getState(id);
        return assetJSON && assetJSON.length > 0;
    }

    // TransferAsset updates the owner field of asset with given id in the world state.
    async TransferAsset(ctx, id, newOwner) {
        const now = new Date();				
        console.log('TransferAsset now: '+ now);		
        const assetString = await this.ReadAsset(ctx, id);
        const asset = JSON.parse(assetString);
        asset.Owner = newOwner;
        return ctx.stub.putState(id, Buffer.from(JSON.stringify(asset)));
    }

    // getAllAssets returns all assets found in the world state.
    async getAllAssets(ctx) {
        const allResults = [];
        // range query with empty string for startKey and endKey does an open-ended query of all assets in the chaincode namespace.
        const iterator = await ctx.stub.getStateByRange('', '');
        let result = await iterator.next();
		let stockid;
        while (!result.done) {
            const strValue = Buffer.from(result.value.value.toString()).toString('utf8');
            let record;
            try {
                record = JSON.parse(strValue);
		        stockid = record.StockID;
                console.log('StockID: '+ stockid);					
            } catch (err) {
                console.log(err);
                record = strValue;
            }
			
            if (stockid !== undefined) {
            allResults.push({ Key: result.value.key, Record: record });
			}			

            result = await iterator.next();
        }
        return JSON.stringify(allResults);
    }
	
////////////////////////////////////////////////////////////////////////////////

    // getAllStocks returns all stocks found in the world state.
    async getAllStocks(ctx) {
        const allResults = [];
        // range query with empty string for startKey and endKey does an open-ended query of all assets in the chaincode namespace.
        const iterator = await ctx.stub.getStateByRange('', '');
        let result = await iterator.next();
		let stockid;
        while (!result.done) {
            const strValue = Buffer.from(result.value.value.toString()).toString('utf8');
            let record;
            try {
                record = JSON.parse(strValue);
		        stockid = record.StockID;
                console.log('StockID: '+ stockid);					
            } catch (err) {
                console.log(err);
                record = strValue;
            }
			
            if (stockid !== undefined) {

            //allResults.push({ Key: result.value.key, Record: record });

            allResults.push(record);
			}			

            result = await iterator.next();
        }
        return JSON.stringify(allResults);
    }	
	
////////////////////////////////////////////////////////////////////////////////

    // getAllContrats returns all contrats found in the world state.
    async getAllContrats(ctx) {
        const allResults = [];
        // range query with empty string for startKey and endKey does an open-ended query of all assets in the chaincode namespace.
        const iterator = await ctx.stub.getStateByRange('', '');
        let result = await iterator.next();
		let contratID;
 
        while (!result.done) {
            const strValue = Buffer.from(result.value.value.toString()).toString('utf8');
            let record;
            try {
                record = JSON.parse(strValue);
		        contratID = record.ContratID;
                console.log('ContratID: '+ contratID);					
            } catch (err) {
                console.log(err);
                record = strValue;
            }
			
            if (contratID !== undefined) {
            //allResults.push({ Key: result.value.key, Record: record });
            allResults.push(record);

			}			

            result = await iterator.next();
        }
        return JSON.stringify(allResults);
    }		
	
////////////////////////////////////////////////////////////////////////////////

    // getAllContratsByStatus returns all contrats by status found in the world state.
    async getAllContratsByStatus(ctx, contratstatus) {
        const allResults = [];
        // range query with empty string for startKey and endKey does an open-ended query of all assets in the chaincode namespace.
        const iterator = await ctx.stub.getStateByRange('', '');
        let result = await iterator.next();
		let contratID;
 		let statusContrat;

        while (!result.done) {
            const strValue = Buffer.from(result.value.value.toString()).toString('utf8');
            let record;
            try {
                record = JSON.parse(strValue);
		        contratID = record.ContratID;
				statusContrat = record.Statut;
                console.log('ContratID: '+ contratID+', Status: '+statusContrat);					
            } catch (err) {
                console.log(err);
                record = strValue;
            }			
            if (contratID !== undefined) {
				if (statusContrat == contratstatus) {
                allResults.push({ Key: result.value.key, Record: record });
				}
			}			
            result = await iterator.next();
        }
        return JSON.stringify(allResults);
    }		
	
////////////////////////////////////////////////////////////////////////////////	

    // getAllContratsByDate returns all contrats by date found in the world state.	
	
    async getAllContratsByDate(ctx, startDate, endDate) {
        const allResults = [];
        // range query with empty string for startKey and endKey does an open-ended query of all assets in the chaincode namespace.
        const iterator = await ctx.stub.getStateByRange('', '');
        let result = await iterator.next();
		let contratID;
 		let dateContrat;

        while (!result.done) {
            const strValue = Buffer.from(result.value.value.toString()).toString('utf8');
            let record;
            try {
                record = JSON.parse(strValue);
		        contratID = record.ContratID;
				dateContrat = record.Datecontrat;
                console.log('ContratID: '+ contratID+', Date: '+dateContrat);					
            } catch (err) {
                console.log(err);
                record = strValue;
            }			
            if (contratID !== undefined) {
				if ((startDate < dateContrat) && (dateContrat < endDate)){
                allResults.push({ Key: result.value.key, Record: record });
				}
			}			
            result = await iterator.next();
        }
        return JSON.stringify(allResults);
    }		
	
////////////////////////////////////////////////////////////////////////////////	
		
	
	
	
}

module.exports = AssetTransfer;
