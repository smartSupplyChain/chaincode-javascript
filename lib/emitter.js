// /home/ope-sboubaker/fabric/fabric-samples/asset-transfer-events/application-javascript

const EventEmitter = require('events');
const emitter = new EventEmitter();

// register a listener
emitter.on('message logged', function(){
	console.log('Listener called');
});

// raise an Event
emitter.emit('message logged');

	